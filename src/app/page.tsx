'use client';
import Slide1 from '@/components/Slides/Slide1';
import Slide2 from '@/components/Slides/Slide2';
// import Slide3 from '@/components/Slides/Slide3';
import Slide4 from '@/components/Slides/Slide4';
import Slide5 from '@/components/Slides/Slide5';
import Slide6 from '@/components/Slides/Slide6';
import Slide7 from '@/components/Slides/Slide7';
import Slide8 from '@/components/Slides/Slide8';
import useKeypress from '@/hooks/useKeyPressed';
import { cn } from '@/utils/cn';
import { motion } from 'framer-motion';
import 'intersection-observer';
import React, { ElementRef } from 'react';

export default function Home() {
  const slidesRef = React.useRef<Map<string, ElementRef<'section'>>>(new Map());

  const currentSlide = React.useRef(0);
  function scrollToCurrentSlide() {
    const slideId = `slide-${currentSlide.current}`;
    if (slidesRef.current && slidesRef.current.has(slideId)) {
      slidesRef.current.get(slideId)!.scrollIntoView({
        behavior: 'smooth',
      });
    }
  }
  useKeypress('ArrowRight', () => {
    currentSlide.current = (currentSlide.current + 1) % slides.length;
    scrollToCurrentSlide();
  });
  useKeypress('ArrowLeft', () => {
    currentSlide.current = (currentSlide.current - 1 + slides.length) % slides.length;
    scrollToCurrentSlide();
  });

  const slides = [Slide1, Slide2, Slide4, Slide5, Slide7, Slide6, Slide8];

  return (
    <div className="h-[100dvh] prose-sm lg:prose-lg xl:prose-xl 2xl:prose-2xl  text-onSurface overflow-hidden">
      {slides.map((Slide, index) => (
        <motion.section
          key={`slide-${index}`}
          ref={(el) => {
            if (slidesRef.current && el) {
              const slideId = `slide-${index}`;
              slidesRef.current.set(slideId, el);
            }
          }}
          id={`slide-${index}`}
          className={cn(
            'w-screen h-[100dvh] relative bg-[#fbfbff]',
            index !== 0 && index !== 4 && 'pt-10',
          )}
        >
          <Slide key={index} />
          {index > 0 && (
            <div className={cn('absolute bottom-5 right-7 flex items-center space-x-6')}>
              <img src="/WINSHOT_PNG.png" className="w-12" />
              <span className="text-2xl pt-5">{index}</span>
            </div>
          )}
        </motion.section>
      ))}
    </div>
  );
}
