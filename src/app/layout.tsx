import { Roboto } from 'next/font/google';
import './globals.css';

const robots = Roboto({
  // weight: ['300', '400', '500', '600', '700'],
  subsets: ['latin'],
  weight: ['100', '300', '400', '500', '700', '900'],
});

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en">
      <body className={robots.className}>
        {/* <Navbar /> */}
        {children}
      </body>
    </html>
  );
}
