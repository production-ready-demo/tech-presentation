import I from '@mdi/react';
// import { IconProps } from '@mdi/react/dist/IconProps';
import { CSSProperties, FunctionComponent, RefObject } from 'react';
interface IconProps {
  id?: string;
  path: string;
  ref?: RefObject<SVGSVGElement>;
  title?: string | null;
  description?: string | null;
  size?: number | string | null;
  color?: string | null;
  horizontal?: boolean;
  vertical?: boolean;
  rotate?: number;
  spin?: boolean | number;
  style?: CSSProperties;
  inStack?: boolean;
  onClick?: (event?: MouseEvent) => void;
  className?: string;
}
const Icon: FunctionComponent<IconProps> = (I as any).default
  ? ((I as any).default as FunctionComponent<IconProps>)
  : I;

export default Icon;
