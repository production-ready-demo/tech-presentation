import Image, { ImageProps } from 'next/image';
import React from 'react';

interface ImgProps extends ImageProps {}
const Img = (props: ImgProps) => {
  return <Image {...props} />;
};

export default Img;
