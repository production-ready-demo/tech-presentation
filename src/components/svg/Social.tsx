export const Linkedin = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      id="Groupe_458"
      data-name="Groupe 458"
      xmlns="http://www.w3.org/2000/svg"
      width="28.834"
      height="28.833"
      viewBox="0 0 28.834 28.833"
      {...props}
    >
      <g id="Groupe_452" data-name="Groupe 452">
        <path
          id="Tracé_3193"
          data-name="Tracé 3193"
          d="M759.163,153.111a14.417,14.417,0,1,1,14.417-14.416A14.433,14.433,0,0,1,759.163,153.111Zm0-27.74a13.324,13.324,0,1,0,13.324,13.324A13.339,13.339,0,0,0,759.163,125.37Z"
          transform="translate(-744.746 -124.277)"
          fill="currentColor"
        />
      </g>
      <g id="Groupe_457" data-name="Groupe 457" transform="translate(7.852 6.689)">
        <g id="Groupe_455" data-name="Groupe 455">
          <g id="Groupe_453" data-name="Groupe 453" transform="translate(0.276 4.637)">
            <rect
              id="Rectangle_16831"
              data-name="Rectangle 16831"
              width="2.871"
              height="9.274"
              fill="currentColor"
            />
          </g>
          <g id="Groupe_454" data-name="Groupe 454">
            <path
              id="Tracé_3194"
              data-name="Tracé 3194"
              d="M757.216,136.876a1.711,1.711,0,1,0-1.7-1.711A1.7,1.7,0,0,0,757.216,136.876Z"
              transform="translate(-755.519 -133.454)"
              fill="currentColor"
            />
          </g>
        </g>
        <g id="Groupe_456" data-name="Groupe 456" transform="translate(4.982 4.353)">
          <path
            id="Tracé_3195"
            data-name="Tracé 3195"
            d="M765.108,144.116c0-1.3.6-2.08,1.749-2.08,1.055,0,1.563.746,1.563,2.08v4.868h2.857v-5.872c0-2.484-1.408-3.685-3.374-3.685a3.234,3.234,0,0,0-2.8,1.532v-1.249h-2.754v9.273h2.754Z"
            transform="translate(-762.354 -139.427)"
            fill="currentColor"
          />
        </g>
      </g>
    </svg>
  );
};

export const Facebook = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      id="Groupe_461"
      data-name="Groupe 461"
      xmlns="http://www.w3.org/2000/svg"
      width="28.833"
      height="28.834"
      viewBox="0 0 28.833 28.834"
      {...props}
    >
      <g id="Groupe_459" data-name="Groupe 459">
        <path
          id="Tracé_3196"
          data-name="Tracé 3196"
          d="M558.671,156.589a14.417,14.417,0,1,1,14.417-14.416A14.433,14.433,0,0,1,558.671,156.589Zm0-27.74a13.324,13.324,0,1,0,13.323,13.324A13.339,13.339,0,0,0,558.671,128.848Z"
          transform="translate(-544.255 -127.755)"
          fill="currentColor"
        />
      </g>
      <g id="Groupe_460" data-name="Groupe 460" transform="translate(10.547 6.033)">
        <path
          id="Tracé_3197"
          data-name="Tracé 3197"
          d="M560.482,152.8h3.376v-8.455h2.356l.251-2.831h-2.607V139.9c0-.668.134-.931.78-.931h1.827v-2.938h-2.338c-2.512,0-3.645,1.106-3.645,3.224v2.257h-1.756v2.867h1.756Z"
          transform="translate(-558.726 -136.033)"
          fill="currentColor"
        />
      </g>
    </svg>
  );
};
export const Youtube = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      id="Groupe_448"
      data-name="Groupe 448"
      xmlns="http://www.w3.org/2000/svg"
      width="28.834"
      height="28.834"
      viewBox="0 0 28.834 28.834"
      {...props}
    >
      <g id="Groupe_445" data-name="Groupe 445">
        <path
          id="Tracé_3189"
          data-name="Tracé 3189"
          d="M825.993,153.01a14.417,14.417,0,1,1,14.417-14.416A14.433,14.433,0,0,1,825.993,153.01Zm0-27.74a13.324,13.324,0,1,0,13.324,13.324A13.339,13.339,0,0,0,825.993,125.269Z"
          transform="translate(-811.577 -124.176)"
          fill="currentColor"
        />
      </g>
      <g id="Groupe_447" data-name="Groupe 447" transform="translate(3.316 6.596)">
        <g id="Groupe_446" data-name="Groupe 446">
          <path
            id="Tracé_3190"
            data-name="Tracé 3190"
            d="M837.865,135.669a2.789,2.789,0,0,0-1.964-1.976c-1.731-.467-8.674-.467-8.674-.467s-6.942,0-8.674.467a2.79,2.79,0,0,0-1.963,1.976,31.425,31.425,0,0,0,0,10.756,2.792,2.792,0,0,0,1.963,1.976c1.732.467,8.674.467,8.674.467s6.943,0,8.674-.467a2.792,2.792,0,0,0,1.964-1.976,31.425,31.425,0,0,0,0-10.756Zm-12.908,8.679v-6.6l5.8,3.3Z"
            transform="translate(-816.127 -133.226)"
            fill="currentColor"
          />
        </g>
      </g>
    </svg>
  );
};
export const Instagram = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      id="Groupe_472"
      data-name="Groupe 472"
      xmlns="http://www.w3.org/2000/svg"
      width="28.834"
      height="28.834"
      viewBox="0 0 28.834 28.834"
      {...props}
    >
      <g id="Groupe_462" data-name="Groupe 462" transform="translate(0)">
        <path
          id="Tracé_3198"
          data-name="Tracé 3198"
          d="M625.5,156.589a14.417,14.417,0,1,1,14.416-14.416A14.433,14.433,0,0,1,625.5,156.589Zm0-27.74a13.324,13.324,0,1,0,13.323,13.324A13.339,13.339,0,0,0,625.5,128.848Z"
          transform="translate(-611.085 -127.755)"
          fill="currentColor"
        />
      </g>
      <g id="Groupe_471" data-name="Groupe 471" transform="translate(6.812 6.812)">
        <g id="Groupe_465" data-name="Groupe 465">
          <g id="Groupe_464" data-name="Groupe 464">
            <g id="Groupe_463" data-name="Groupe 463">
              <path
                id="Tracé_3199"
                data-name="Tracé 3199"
                d="M628.036,138.471c2.031,0,2.271.008,3.073.044a4.219,4.219,0,0,1,1.412.262,2.519,2.519,0,0,1,1.443,1.443,4.2,4.2,0,0,1,.262,1.412c.036.8.044,1.043.044,3.074s-.008,2.271-.044,3.073a4.2,4.2,0,0,1-.262,1.412,2.519,2.519,0,0,1-1.443,1.443,4.2,4.2,0,0,1-1.412.262c-.8.036-1.042.044-3.073.044s-2.271-.008-3.074-.044a4.2,4.2,0,0,1-1.412-.262,2.514,2.514,0,0,1-1.443-1.443,4.215,4.215,0,0,1-.262-1.412c-.036-.8-.044-1.042-.044-3.073s.008-2.271.044-3.074a4.214,4.214,0,0,1,.262-1.412,2.514,2.514,0,0,1,1.443-1.443,4.214,4.214,0,0,1,1.412-.262c.8-.036,1.043-.044,3.074-.044m0-1.37c-2.066,0-2.324.009-3.135.046a5.588,5.588,0,0,0-1.846.353,3.893,3.893,0,0,0-2.224,2.224,5.584,5.584,0,0,0-.354,1.846c-.037.811-.046,1.07-.046,3.135s.009,2.324.046,3.135a5.584,5.584,0,0,0,.354,1.846,3.893,3.893,0,0,0,2.224,2.224,5.607,5.607,0,0,0,1.846.353c.811.036,1.07.045,3.135.045s2.324-.009,3.135-.045a5.606,5.606,0,0,0,1.846-.353,3.889,3.889,0,0,0,2.224-2.224,5.569,5.569,0,0,0,.353-1.846c.037-.811.046-1.07.046-3.135s-.009-2.324-.046-3.135a5.569,5.569,0,0,0-.353-1.846,3.889,3.889,0,0,0-2.224-2.224,5.587,5.587,0,0,0-1.846-.353c-.811-.036-1.07-.046-3.135-.046"
                transform="translate(-620.431 -137.101)"
                fill="currentColor"
              />
            </g>
          </g>
        </g>
        <g id="Groupe_468" data-name="Groupe 468" transform="translate(3.7 3.7)">
          <g id="Groupe_467" data-name="Groupe 467">
            <g id="Groupe_466" data-name="Groupe 466">
              <path
                id="Tracé_3200"
                data-name="Tracé 3200"
                d="M629.413,142.178a3.905,3.905,0,1,0,3.905,3.905,3.905,3.905,0,0,0-3.905-3.905m0,6.44a2.535,2.535,0,1,1,2.535-2.535,2.535,2.535,0,0,1-2.535,2.535"
                transform="translate(-625.508 -142.178)"
                fill="currentColor"
              />
            </g>
          </g>
        </g>
        <g id="Groupe_470" data-name="Groupe 470" transform="translate(10.752 2.633)">
          <g id="Groupe_469" data-name="Groupe 469">
            <path
              id="Tracé_3201"
              data-name="Tracé 3201"
              d="M637.009,141.627a.913.913,0,1,1-.913-.913.913.913,0,0,1,.913.913"
              transform="translate(-635.183 -140.714)"
              fill="currentColor"
            />
          </g>
        </g>
      </g>
    </svg>
  );
};
