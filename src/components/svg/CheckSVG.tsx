export const CheckSVG = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="16.717"
      height="13.276"
      viewBox="0 0 16.717 13.276"
      {...props}
    >
      <path
        id="Tracé_5100"
        data-name="Tracé 5100"
        d="M3.3,66.423a.492.492,0,0,0-.7,0L.144,68.881a.492.492,0,0,0,0,.7l5.9,5.9a.492.492,0,0,0,.7,0l9.834-9.834a.492.492,0,0,0,0-.7l-2.458-2.458a.492.492,0,0,0-.7,0L6.392,69.517Z"
        transform="translate(0 -62.345)"
        fill="currentColor"
        fillRule="evenodd"
      />
    </svg>
  );
};
