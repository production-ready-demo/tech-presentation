export const CurvedLine = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="104.572"
      height="55.059"
      viewBox="0 0 104.572 55.059"
      {...props}
    >
      <path
        id="Tracé_5033"
        data-name="Tracé 5033"
        d="M1100.779,506.424s55.815-8.878,46.64,8.751-3.058,25.618,12.743,9.639,19.115-12.556,19.115-.761,3.058,21.18,15.8,8.244"
        transform="matrix(-0.985, 0.174, -0.174, -0.985, 1273.577, 341.603)"
        fill="none"
        stroke="currentColor"
        strokeLinecap="round"
        strokeWidth={4}
      />
    </svg>
  );
};
export const CurvedLineVertical = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="186.018"
      height="202.026"
      viewBox="0 0 186.018 202.026"
      {...props}
    >
      <path
        id="Tracé_5104"
        data-name="Tracé 5104"
        d="M1100.779,511.749s93.94-30.026,78.5,29.6-5.147,86.648,21.448,32.6,32.171-42.466,32.171-2.574,5.147,71.635,26.595,27.882"
        transform="translate(196.176 1404.246) rotate(-120)"
        fill="none"
        stroke="currentColor"
        strokeLinecap="round"
        strokeWidth={4}
      />
    </svg>
  );
};

export const CurvedLineFullPage = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="2208.226"
      height="437.4"
      viewBox="0 0 2208.226 437.4"
      {...props}
    >
      <path
        id="Tracé_5163"
        data-name="Tracé 5163"
        d="M4257.707,8396.439s-425.136,0-552.042,84.076-439.413,231.6-161.806,299.815,563.147,61.867,556.8-52.349-363.27-341.06-653.568-231.6S3179,8875.511,2901.4,8823.162s-347.406-353.75-109.457-342.646,0,149.114,0,149.114-450.517,302.989-452.1,128.493-209.4,161.806-288.712,47.59"
        transform="translate(-2049.481 -8394.439)"
        fill="none"
        stroke="currentColor"
        strokeWidth={4}
      />
    </svg>
  );
};
