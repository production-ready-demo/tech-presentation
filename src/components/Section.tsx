import React from 'react';
import { cn } from '@/utils/cn';
//create the button Props
export interface SectionProps extends React.ComponentPropsWithoutRef<'section'> {
  startIcon?: React.ReactNode;
  //write other extra props here...
}
const Section = ({ className, ...rest }: React.PropsWithChildren<SectionProps>) => {
  return <section className={cn('flex-1 h-full pl-10', className)} {...rest} />;
};

export default Section;
