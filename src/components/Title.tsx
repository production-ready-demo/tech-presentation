import { cn } from '@/utils/cn';
import React, { HTMLAttributes } from 'react';
interface TitleProps extends HTMLAttributes<HTMLDivElement> {}
const Title = ({ className, ...props }: TitleProps) => {
  return <h1 className={cn('font-bold', className)} {...props} />;
};

export default Title;
