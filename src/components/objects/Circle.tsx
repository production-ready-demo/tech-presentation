import { cn } from '@/utils/cn';
import React from 'react';
interface CircleProps extends React.ComponentPropsWithoutRef<'div'> {}
const Circle = ({ className, ...rest }: CircleProps) => {
  return <div className={cn('rounded-full', className)} {...rest} />;
};

export default Circle;
