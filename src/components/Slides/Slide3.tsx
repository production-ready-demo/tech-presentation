import { cn } from '@/utils/cn';
import Section from '../Section';
import Title from '../Title';

const Slide3 = () => {
  const items = [
    {
      title: 'Event 1',
      cardTitle: 'Card Title 1',
      cardSubtitle: 'Card Subtitle 1',
      cardDetailedText: 'Card Detailed Text 1',
      media: {
        type: 'IMAGE',
        source: {
          url: 'https://picsum.photos/id/1018/1000',
        },
      },
    },
    {
      title: 'Event 2',
      cardTitle: 'Card Title 2',
      cardSubtitle: 'Card Subtitle 2',
      cardDetailedText: 'Card Detailed Text 2',
      media: {
        type: 'IMAGE',
        source: {
          url: 'https://picsum.photos/id/1015/1000',
        },
      },
    },
  ];

  return (
    <Section className={cn('grid grid-cols-1')}>
      <Title>Our Journey</Title>
    </Section>
  );
};

export default Slide3;
