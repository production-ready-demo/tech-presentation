import { cn } from '@/utils/cn';
import Section from '../Section';
import Title from '../Title';

// @ts-ignore
const Slide8 = () => {
  return (
    <Section className={cn('space-y-8 flex-1')}>
      <div className="relative flex items-center justify-between mr-10">
        <Title>Q & A</Title>
      </div>
      <div className="flex items-center justify-center flex-1 h-[50dvh]">
        <a
          href="https://gitlab.com/production-ready-demo/tech-presentation"
          className="text-blue"
          target="_blank"
        >
          https://gitlab.com/production-ready-demo/tech-presentation
        </a>
      </div>
    </Section>
  );
};

export default Slide8;
