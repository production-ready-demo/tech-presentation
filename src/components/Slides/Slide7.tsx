import useKeypress from '@/hooks/useKeyPressed';
import { cn } from '@/utils/cn';
import { AnimatePresence, motion } from 'framer-motion';
import { useRef, useState } from 'react';
import Section from '../Section';
import Title from '../Title';

// @ts-ignore
import { useIsVisible } from 'react-is-visible';
const Slide7 = () => {
  const sectionRef = useRef<HTMLDivElement>(null);
  const isVisible = useIsVisible(sectionRef);

  const [currentStep, setCurrentStep] = useState(0);
  useKeypress('n', (e) => {
    if (!isVisible) {
      return;
    }
    setCurrentStep((prevStep) => {
      if (prevStep === 1) {
        return prevStep;
      }
      return prevStep + 1;
    });
  });
  useKeypress('p', (e) => {
    if (!isVisible) {
      return;
    }
    setCurrentStep((prevStep) => {
      if (prevStep === 0) {
        return prevStep;
      }
      return prevStep - 1;
    });
  });
  return (
    <Section
      className={cn('bg-contain h-screen bg-center', { 'bg-no-repeat': true })}
      style={{ backgroundImage: "url('/meme.png')" }}
    >
      <div className="flex grow flex-col justify-between">
        <div className="py-10">
          <div ref={sectionRef} className="text-center bg-onPrimary bg-opacity-30">
            <h1 className="text-white font-bold px-4">
              WORKED FINE IN DEV <br /> IT'S A <span className="text-red">DEVOPS</span> PROBLEM NOW.
            </h1>
          </div>
        </div>
      </div>
    </Section>
  );
};

export default Slide7;
