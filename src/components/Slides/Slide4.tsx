import { cn } from '@/utils/cn';
import Section from '../Section';
import Title from '../Title';
import { useRef, useState } from 'react';
// @ts-ignore
import { useIsVisible } from 'react-is-visible';
import useKeypress from '@/hooks/useKeyPressed';
import { AnimatePresence, motion } from 'framer-motion';

function Slide4() {
  const sectionRef = useRef<HTMLDivElement>(null);
  const isVisible = useIsVisible(sectionRef);
  const [currentStep, setCurrentStep] = useState(0);
  useKeypress('n', (e) => {
    if (!isVisible) {
      return;
    }
    setCurrentStep((prevStep) => {
      if (prevStep === 2) {
        return prevStep;
      }
      return prevStep + 1;
    });
  });
  useKeypress('p', (e) => {
    if (!isVisible) {
      return;
    }
    setCurrentStep((prevStep) => {
      if (prevStep === 0) {
        return prevStep;
      }
      return prevStep - 1;
    });
  });
  return (
    <Section className={cn('space-y-4')}>
      <div ref={sectionRef} className="relative flex items-center justify-between mr-10">
        <Title>Our Technology Stack</Title>
        <img src="/wired-outline-12-layers.gif" className="absolute h-48 right-10 -top-24" />
      </div>
      <div className="flex items-center justify-around h-full pb-20 mr-10 space-x-4">
        <AnimatePresence>
          {currentStep >= 0 && (
            <motion.div
              key="slide-4-step1"
              initial={{ opacity: 0, x: 100 }}
              animate={{ opacity: 1, x: 0 }}
              exit={{ opacity: 0, x: 100 }}
              transition={{ duration: 1 }}
              className="relative w-1/3 h-[26rem] border rounded-md shadow-md"
            >
              <div className="py-2 bg-primary rounded-t-md">
                <h5 className="font-medium text-center text-onPrimaryContainer">
                  Web/Mobile Development
                </h5>
              </div>
              <ul className="list-decimal">
                <li className="">Javascript / Typescript</li>
                <li className="">React</li>
                <li className="">NextJs as Framework</li>
                <li className="">
                  React Native <span className="font-bold">(Mobile App)</span>
                </li>
                <li className="">Web RTC</li>
                <li className="">
                  Nginx <span className="font-bold">(Web Server & CDN)</span>
                </li>
              </ul>
              <img
                src="/Nginx-Logo-02.png"
                className="absolute inset-x-0 z-0 w-24 mx-auto top-1/3 opacity-30"
              />
            </motion.div>
          )}
          {currentStep >= 1 && (
            <motion.div
              key="slide-4-step2"
              initial={{ opacity: 0, x: 100 }}
              animate={{ opacity: 1, x: 0 }}
              exit={{ opacity: 0, x: 50 }}
              transition={{ duration: 1 }}
              className="relative w-1/3 border h-[26rem] rounded-md shadow-md"
            >
              <div className="py-2 rounded-t-md bg-palette2">
                <h5 className="font-medium text-center grow text-onPrimaryContainer">
                  Backend Development
                </h5>
              </div>
              <ul className="list-decimal">
                <li className="">NodeJs</li>
                <li className="">
                  Express <span className="font-bold">(as Framework)</span>
                </li>
                <li className="">
                  Redis <span className="font-bold">(Caching)</span>
                </li>
                <li className="">
                  RabitMQ <span className="font-bold">(Services communication)</span>
                </li>
                <li className="">
                  ... <span>(and more)</span>
                </li>
              </ul>
            </motion.div>
          )}
          {currentStep >= 2 && (
            <motion.div
              key="slide-4-step3"
              initial={{ opacity: 0, x: 100 }}
              animate={{ opacity: 1, x: 0 }}
              exit={{ opacity: 0, x: 50 }}
              transition={{ duration: 1 }}
              className="relative w-1/3 border h-[26rem] rounded-md shadow-md"
            >
              <div className="py-2 rounded-t-md bg-palette3">
                <h5 className="font-medium text-center grow text-onPrimaryContainer">
                  Cloud/DevOps
                </h5>
              </div>
              <ul className="list-decimal">
                <li className="">Gitlab</li>
                <li className="">AWS EC2</li>
                <li>
                  Nginx <b>(Load Balancer)</b>
                </li>
                <li>AWS S3</li>
                <li>AWS Cloud Watch</li>
                <li>
                  CircleCI <b>(CI/CD)</b>
                </li>
                <li>
                  ... <span>(and more)</span>
                </li>
              </ul>
              <img
                src="/AWS_Simple_Icons_AWS_Cloud.svg.png"
                className="absolute inset-x-0 z-0 mx-auto rotate-45 w-36 top-1/3 opacity-30"
              />
            </motion.div>
          )}
        </AnimatePresence>
      </div>
    </Section>
  );
}

export default Slide4;
