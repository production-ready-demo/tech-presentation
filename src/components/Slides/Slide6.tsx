import useKeypress from '@/hooks/useKeyPressed';
import { cn } from '@/utils/cn';
import { AnimatePresence, motion } from 'framer-motion';
import { useRef, useState } from 'react';
import Section from '../Section';
import Title from '../Title';

// @ts-ignore
import { useIsVisible } from 'react-is-visible';
const Slide6 = () => {
  const sectionRef = useRef<HTMLDivElement>(null);
  const isVisible = useIsVisible(sectionRef);

  const [currentStep, setCurrentStep] = useState(0);
  useKeypress('n', (e) => {
    if (!isVisible) {
      return;
    }
    setCurrentStep((prevStep) => {
      if (prevStep === 1) {
        return prevStep;
      }
      return prevStep + 1;
    });
  });
  useKeypress('p', (e) => {
    if (!isVisible) {
      return;
    }
    setCurrentStep((prevStep) => {
      if (prevStep === 0) {
        return prevStep;
      }
      return prevStep - 1;
    });
  });
  return (
    <Section className={cn('space-y-8 flex-1')}>
      <div ref={sectionRef} className="relative flex items-center justify-between mr-10">
        <Title>Live Demo</Title>
      </div>
      <div className="flex items-start justify-between flex-1 mr-10 space-x-4">
        <div className="shrink-0">
          <ul className="space-y-10 text-2xl list-decimal">
            <li>
              Manage <b className="highlight">releases</b>
            </li>
            <li>Build docker images (Gitlab CI)</li>
            <li>
              Create <b className="highlight">pipelines</b> based on releases
            </li>
            <li>
              Deploy releases <b className="highlight">(CD)</b>
            </li>
            <li>Manage environment variables</li>
            <li>Blue Green deployment</li>
          </ul>
        </div>
        <div className="flex-1">
          <AnimatePresence>
            {currentStep > 0 && (
              <motion.img
                whileHover={{
                  scale: 1.2,
                  x: -100,
                  y: -50,
                }}
                initial={{ opacity: 0, x: 200 }}
                animate={{ opacity: 1, x: 0 }}
                exit={{ opacity: 0, x: 100 }}
                transition={{ duration: 1 }}
                src="/presentation-diagram.svg"
                className="flex-1 w-full hover:bg-white hover:shadow-lg"
              />
            )}
          </AnimatePresence>
        </div>
      </div>
    </Section>
  );
};

export default Slide6;
