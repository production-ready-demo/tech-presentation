import { cn } from '@/utils/cn';
import Section from '../Section';
import Title from '../Title';
import Link from 'next/link';
const Slide2 = () => {
  return (
    <Section className={cn('section4 flex justify-between mr-10')}>
      <div className="flex flex-col mb-10">
        <Title>Roadmap</Title>
        <ul className="space-y-10 text-2xl list-item">
          {/* <li className="">
            I- Our journey <span className="text-gray-500">- 5 mins</span>
          </li> */}
          <li>
            I- Our Technology Stack <span className="text-gray-500">- 5 mins</span>
          </li>
          <li>
            II- Our Software Development <span className="font-bold highlight">Lifecycle</span>{' '}
            <span className="text-gray-500">- 5 mins</span>
          </li>
          <li>
            III- Live Demo <span className="text-gray-500">- 20 mins</span>
          </li>
          <li>
            VI- <span className="highlight">Q/A</span>
            <span className="text-gray-500">- 5 mins</span>
          </li>
        </ul>
      </div>
      <div className="flex flex-col items-center pt-0 space-y-2">
        <img src="/me.jpeg" className="w-56 border-2 border-white rounded-full bg-red" />
        <h2 className="font-bold">Amjed Bouhouch</h2>
        <h4>
          Technical lead / Co-Founder{' '}
          <Link
            className="underline text-primary"
            href={'https://www.winshot.net'}
            rel="noreferrer"
            target={'_blank'}
          >
            @Winshot
          </Link>{' '}
          - Retail Excellence
        </h4>
        <div className="space-y-0">
          <span className="flex flex-row items-center space-x-2">
            <span>
              Email :{''}
              <Link className="underline pl-7" href={"mailto:'amjed@winshot.net'"} rel="noreferrer">
                amjed@winshot.net
              </Link>
            </span>
          </span>
          <span className="flex flex-row items-center space-x-2">
            <span>
              Github :{''}
              <Link
                className="pl-5 underline"
                href={'https://github.com/amjadbouhouch'}
                rel="noreferrer"
                target={'_blank'}
              >
                amjadbouhouch
              </Link>
            </span>
          </span>
          <span className="flex flex-row items-center space-x-2">
            <span>
              Linkedin :{' '}
              <Link
                className="underline"
                href={'https://www.linkedin.com/in/amjedbouhouch/'}
                rel="noreferrer"
                target={'_blank'}
              >
                amjedbouhouch
              </Link>
            </span>
          </span>
        </div>
      </div>
    </Section>
  );
};

export default Slide2;
