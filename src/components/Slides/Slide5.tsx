import useKeypress from '@/hooks/useKeyPressed';
import { cn } from '@/utils/cn';
import { AnimatePresence, motion } from 'framer-motion';
import { useRef, useState } from 'react';
import Section from '../Section';
import Title from '../Title';

// @ts-ignore
import { useIsVisible } from 'react-is-visible';
const Slide5 = () => {
  const sectionRef = useRef<HTMLDivElement>(null);
  const isVisible = useIsVisible(sectionRef);

  const [currentStep, setCurrentStep] = useState(0);
  useKeypress('n', (e) => {
    if (!isVisible) {
      return;
    }
    setCurrentStep((prevStep) => {
      if (prevStep === 4) {
        return prevStep;
      }
      return prevStep + 1;
    });
  });
  useKeypress('p', (e) => {
    if (!isVisible) {
      return;
    }
    setCurrentStep((prevStep) => {
      if (prevStep === 0) {
        return prevStep;
      }
      return prevStep - 1;
    });
  });
  return (
    <Section className={cn('space-y-8 flex-1')}>
      <div ref={sectionRef} className="relative flex items-center justify-between mr-10">
        <Title>
          Software Development <span className="highlight">Lifecycle</span>
        </Title>
        <img src="/wired-outline-1639-stairs.gif" className="absolute h-48 right-10 -top-20" />
      </div>
      <div className="flex items-center justify-around flex-1 mr-10 space-x-4 pb-28 h-5/6">
        <AnimatePresence>
          {currentStep >= 0 && (
            <motion.div
              key="step1"
              initial={{ opacity: 0, x: -50 }}
              animate={{ opacity: 1, x: 0 }}
              exit={{ opacity: 0, x: 50 }}
              transition={{ duration: 1 }}
              className="flex flex-col items-center step1"
            >
              <img src="/wired-outline-134-target.gif" className="w-28" />
              <h6 className="font-medium">1- Requirement & Analysis</h6>
            </motion.div>
          )}

          {currentStep >= 1 && (
            <motion.div
              key="step2"
              initial={{ opacity: 0, x: -50 }}
              animate={{ opacity: 1, x: 0 }}
              exit={{ opacity: 0, x: 50 }}
              transition={{ duration: 1 }}
              className="flex justify-end h-full flex-col items-center step²"
            >
              <div className="flex items-center">
                <img src="/system-regular-23-calendar.gif" className="w-28" />
                <img src="/wired-outline-680-it-developer.gif" className="w-28" />
              </div>
              <h6 className="font-medium">2- Technical Design & Planning</h6>
            </motion.div>
          )}
          {currentStep >= 2 && (
            <motion.div
              key="step3"
              initial={{ opacity: 0, x: -50 }}
              animate={{ opacity: 1, x: 0 }}
              exit={{ opacity: 0, x: 50 }}
              transition={{ duration: 1 }}
              className="flex flex-col items-center"
            >
              <img src="/wired-outline-1326-command-window-line.gif" className="w-28" />
              <h6 className="font-medium">3- Product Development</h6>
            </motion.div>
          )}
          {currentStep >= 3 && (
            <motion.div
              key="step4"
              initial={{ opacity: 0, x: -50 }}
              animate={{ opacity: 1, x: 0 }}
              exit={{ opacity: 0, x: 50 }}
              transition={{ duration: 1 }}
              className="flex flex-col items-center justify-end h-full"
            >
              <img src="/wired-outline-1331-repository.gif" className="w-28" />
              <h6 className="font-medium">4- Integration & Testing</h6>
            </motion.div>
          )}
          {currentStep >= 4 && (
            <motion.div
              key="step5"
              initial={{ opacity: 0, x: -50 }}
              animate={{ opacity: 1, x: 0 }}
              exit={{ opacity: 0, x: 50 }}
              transition={{ duration: 1 }}
              className="flex flex-col items-center space-y-0"
            >
              <img src="/wired-outline-1103-confetti.gif" className="w-28" />
              <h6 className="font-medium">5- Deployment & Maintenance</h6>
            </motion.div>
          )}
        </AnimatePresence>
        <img src="/test.svg" className="absolute w-full opacity-10 text-palette2" />
      </div>
    </Section>
  );
};

export default Slide5;
