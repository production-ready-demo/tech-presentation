import { cn } from '@/utils/cn';
import Section from '../Section';
import Circle from '../objects/Circle';
import Title from '../Title';
interface Slide1Props {}
const Slide1 = ({}: Slide1Props) => {
  return (
    <Section className={cn('grid grid-cols-1 gap-5 relative', 'lg:grid-cols-2')}>
      <img src="/icon_winshot.png" className="absolute w-60 right-10" />
      <div className="flex flex-col justify-center space-y-8">
        <img src="/wired-outline-2234-firework.gif" className="w-40" />
        <Title>From Concept to Launch</Title>
        <h3 className="font-medium leading-loose">
          A Guide to the <span className="highlight">Software Development</span> Lifecycle <br />
          <span className="highlight">DevOps</span>, and Effective Collaboration
        </h3>
      </div>
      <div className="flex flex-col items-center justify-center space-y-8"></div>
    </Section>
  );
};

export default Slide1;
