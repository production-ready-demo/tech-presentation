# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.4](https://gitlab.com/gofield/spotly/web-site/compare/stable-0.1.3...stable-0.1.4) (2023-11-10)

### [0.1.3](https://gitlab.com/gofield/spotly/web-site/compare/stable-0.1.2...stable-0.1.3) (2023-11-10)


### Features

* add seo ([fc7c029](https://gitlab.com/gofield/spotly/web-site/commit/fc7c0291afa6d7bfbce7210926c4ccede2f0a39d))

### [0.1.2](https://gitlab.com/gofield/spotly/web-site/compare/stable-0.1.1...stable-0.1.2) (2023-11-07)


### Features

* add sitemap ([5ef11ac](https://gitlab.com/gofield/spotly/web-site/commit/5ef11ac9a4cbf860273170814452f6c39216f2b5))

### 0.1.1 (2023-11-03)
