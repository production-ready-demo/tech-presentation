FROM node:18.17.1-alpine as builder

WORKDIR /web-site
COPY package*.json ./
COPY yarn.lock ./

# RUN npm i -g yarn
RUN yarn install --immutable
COPY . .
RUN yarn build


# production environment
FROM nginx:1.21.0-alpine
# copy config files
COPY /.nginx/nginx.conf /etc/nginx/conf.d/default.conf
# copy build files
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /web-site/out /usr/share/nginx/html
## Export port 80
EXPOSE 80
ENTRYPOINT ["nginx", "-g", "daemon off;"]
